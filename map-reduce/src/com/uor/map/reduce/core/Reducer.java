package com.uor.map.reduce.core;


import java.util.Map;

public abstract class Reducer {
	// Intermediate records for this reducer instance to process
    public Map.Entry records;

    // Default constructor
    public Reducer() {}

    // Setters
    public void setRecords(Map.Entry records) {
        this.records = records;
    }

    // Execute the reduce function for each key-value pair in the intermediate results output by the mapper
    public void run() {	

    	Map.Entry m = (Map.Entry)records;   	
    	reduce(m.getKey(), m.getValue());	 // execute the reduce function		     		
		
    }

    // Abstract reduce function to the overwritten by objective-specific class
    public abstract void reduce(Object key, Object values);

}
