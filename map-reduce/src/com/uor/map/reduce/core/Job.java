package com.uor.map.reduce.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import com.uor.map.reduce.model.Airport;
import com.uor.map.reduce.model.Passenger;

public class Job {
	
	// Job configuration
    private Config config;
    
    // Global object to store intermediate and final results
    protected static Map mapOutput;
        
    protected static Map reducerOutput;


    public static Map getMapOutput() {
		return mapOutput;
	}

    public static void setMapOutput(Map mapOutput) {
		Job.mapOutput = mapOutput;
	}

    public static Map getReducerOutput() {
		return reducerOutput;
	}

    public static void setReducerOutput(Map reducerOutput) {
		Job.reducerOutput = reducerOutput;
	}

	// Constructor
    public Job(Config config) {
        this.config = config;
    }

    // Run the job given the provided configuration
    public void run() throws Exception {
    	
        // Initialise the map to store intermediate results
    	setReducerOutput(new HashMap());
    	setMapOutput(new HashMap());

        map();
        reduce();        
    }

    // Map each provided file using an instance of the mapper specified by the job configuration
    private void map() throws Exception {
    	
    	List<Passenger> passengerList = new ArrayList<Passenger>();
    	List<Airport> airportList = new ArrayList<Airport>();
    	
    	passengerList = config.getPassengerList();
    	airportList = config.getAirportList();
    	
    	Mapper mapper = config.getMapperInstance();   
    	mapper.setFile(passengerList, airportList);
    	mapper.run();
        System.out.println("***********************Completed Map Phase**********************");
        
    }
    
    // Reduce the intermediate results output by the map phase using an instance of the reducer specified by the job configuration
    private void reduce() throws Exception {
    	
    	Map m = new HashMap();
    	m = getMapOutput();
    	Reducer reducer = config.getReducerInstance(); // get reducer instance
    	Iterator it = m.entrySet().iterator();
    	while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            reducer.setRecords(pair);      
            reducer.run();							// execute reducer for each key-value pair
        }
    	
	
        System.out.println("***********************Completed Reducer Phase**********************");
    }
    
    
}
