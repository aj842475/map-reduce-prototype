package com.uor.map.reduce.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.uor.map.reduce.core.Combiner;
import com.uor.map.reduce.core.Job;
import com.uor.map.reduce.core.Mapper;
import com.uor.map.reduce.core.Reducer;
import com.uor.map.reduce.model.Airport;
import com.uor.map.reduce.model.ModelObj1;
import com.uor.map.reduce.model.ModelObj2;
import com.uor.map.reduce.model.Passenger;

public class Obj1 {
	
	public static class MapperObj1 extends Mapper {
		synchronized public void map(Object obj1, Object obj2) { 
//			System.out.println("1 mapper");
			Map map = Collections.synchronizedMap(new HashMap());
			
			map = Job.getMapOutput();
//			System.out.println("each map : "+map);
			List<Passenger> p = (List<Passenger>)obj1;
			List<Airport> a = (List<Airport>)obj2;
			
			
			for(int i=0;i<p.size();i++) {
				String key = p.get(i).getFromAirportCode();
				Airport filteredAirport = a.stream().filter(value -> value.getAirportCode().equals(key)).findAny().orElse(null);
				
//				System.out.println("key : "+key);
				List<Integer> count = new ArrayList<Integer>();					
		        if(map.containsKey(key)) {
		        	ModelObj1 values = (ModelObj1) map.get(key);
		            values.getFlights().add(1);
		            map.replace(key, values);
		        }else {
		        	if(count != null) {
		        		count.add(1);
			        }
		        	ModelObj1 mo1 = new ModelObj1();
		        	mo1.setFlights(count);
		        	if(filteredAirport != null) {
		        		mo1.setAirportName(filteredAirport.getAirportName());
		        	}		        	
		        	else {
		        		System.out.println("Airport : "+key+" not found!");
		        	}
		        	map.put(key, mo1);
		        }
			}
			
			for (Iterator iterator = a.iterator(); iterator.hasNext();) {
				Airport airport = (Airport) iterator.next();
				
				if(airport != null) {
//					System.out.println("airport : "+airport.toString());
					if(!map.containsKey(airport.getAirportCode())) {
						List values = new ArrayList<>();
						values.add(0);
						ModelObj1 mo1 = new ModelObj1();
						mo1.setFlights(values);
						mo1.setAirportName(airport.getAirportName());
						map.put(airport.getAirportCode(), mo1);
					}
				}
				
			}
			
			Job.setMapOutput(map);
			
		}
		
    }
	
	public static class CombinerObj1 extends Combiner {
		public void combiner(Object key, Object value) {
			System.out.println("1 combiner");
			List values;
	        if(Job.getCombinerOutput().containsKey(key)) {
	            values = (List) Job.getMapOutput().get(key);
	        } else {
	            values = new ArrayList<>();
	            Job.getCombinerOutput().put(key, values);
	        }
	        values.add(value);
	  	}
	}
	
    public static class ReducerObj1 extends Reducer {
    	synchronized public void reduce(Object key, Object values) {
//        	System.out.println("1 reducer");   
//        	System.out.println("key : "+key);
        	int count = 0;
        	Map m = Job.getReducerOutput();
        	
    		List vals = new ArrayList();
    		ModelObj1 m1 = (ModelObj1)values;
    		vals = m1.getFlights();
//        	vals = (ModelObj1)values;
            for (Object value : vals) {
            	count += (int) value;
            }
            
            m1.setFlightCount(count);
//            System.out.println("key : "+key+" "+"val : "+count);
            m.put(key, m1);
            Job.setReducerOutput(m);
		}  
        	
    	
    }
}
