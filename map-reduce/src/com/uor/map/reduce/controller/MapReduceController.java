package com.uor.map.reduce.controller;



import java.util.Map;

import com.uor.map.reduce.controller.Obj1.MapperObj1;
import com.uor.map.reduce.controller.Obj1.ReducerObj1;
import com.uor.map.reduce.controller.Obj2.MapperObj2;
import com.uor.map.reduce.controller.Obj2.ReducerObj2;
import com.uor.map.reduce.controller.Obj3.MapperObj3;
import com.uor.map.reduce.controller.Obj3.ReducerObj3;
import com.uor.map.reduce.core.Config;
import com.uor.map.reduce.core.Job;
import com.uor.map.reduce.facade.CommonFacade;
import com.uor.map.reduce.facade.FileProcessingFacade;



/**
 * Controls the main thread
 *
 */
public class MapReduceController {

	public static void main(String[] args) throws Exception {
		
		// data cleaning and error processing
		Map processedFiles = FileProcessingFacade.errorProcessing();
		
		/**************** START JOBS EXECUTION **************/
        Config config1 = new Config(processedFiles, MapperObj1.class, ReducerObj1.class);
        Job job1 = new Job(config1);
        job1.run();
		Map job1Op =  job1.getReducerOutput();
		
		Config config2 = new Config(processedFiles, MapperObj2.class, ReducerObj2.class);
		Job job2 = new Job(config2); 
		job2.run();
		Map job2Op =  job2.getReducerOutput();

		Config config3 = new Config(processedFiles, MapperObj3.class, ReducerObj3.class);
		Job job3 = new Job(config3); 
		job3.run();
		Map job3Op =  job3.getReducerOutput();
		/**************** END JOBS EXECUTION **************/

		// generate and export output PDF report
		CommonFacade.exportOutputReportPDF(processedFiles, job1Op, job2Op, job3Op);

    }
}
