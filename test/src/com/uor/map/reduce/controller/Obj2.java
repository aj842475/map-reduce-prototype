package com.uor.map.reduce.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.uor.map.reduce.core.Job;
import com.uor.map.reduce.core.Mapper;
import com.uor.map.reduce.core.Reducer;
import com.uor.map.reduce.model.ModelObj2;
import com.uor.map.reduce.model.Passenger;

public class Obj2 {
	public static class MapperObj2 extends Mapper {
		public void map(Object obj1, Object obj2) { 
//			System.out.println("2 mapper");
			List<Passenger> p = (List<Passenger>)obj1;
			Map map = new HashMap();
			map = Job.getMapOutput();
			for(int i=0;i<p.size();i++) {
				String key = p.get(i).getFlightId();
//				System.out.println("key : "+key);
				List values = new ArrayList<>();					
		        if(map.containsKey(key)) {
		            values = (List) map.get(key);
		            values.add(p.get(i));
		            map.replace(key, values);
		        }else {
		        	if(values != null) {
			        	values.add(p.get(i));
			        }
		        	map.put(key, values);
		        }
			}
			Job.setMapOutput(map);
		}
    }
    // Word count reducer:
    // Output the total number of occurrences of each unique word
    // KEY = word
    // VALUE = count
	public static class ReducerObj2 extends Reducer {
    	public void reduce(Object key, Object values) {
//        	System.out.println("1 reducer");

        	Map m = Job.getReducerOutput();
        	ModelObj2 mo = new ModelObj2();
        	
    		List vals = new ArrayList();
    		List<Passenger> pList = new ArrayList<Passenger>();
        	vals = (ArrayList)values;
        	mo.setPassengerCount(vals.size());
            for (Object value : vals) {
            	Passenger p = (Passenger)value;
            	p.setArrivalTime(p.getDepartureTime(), p.getTotalFlightTime());
            	pList.add(p);
            }
            mo.setPassengerList(pList);
//            System.out.println("key : "+key+" "+"val : "+mo.toString());
            m.put(key, mo);
            Job.setReducerOutput(m);
    	}
    }
}
