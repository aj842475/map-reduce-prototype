package com.uor.map.reduce.facade;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.text.similarity.LevenshteinDistance;

import com.uor.map.reduce.constants.Constants;
import com.uor.map.reduce.model.Airport;
import com.uor.map.reduce.model.ErrorData;
import com.uor.map.reduce.model.Passenger;



/**
 * Process files
 *
 */
public class FileProcessingFacade {
	
	/**
	 * Read the aiport file, passenger file with and without error
	 */
	public static Map readFiles() {
		
		List<Passenger> noErrorPassengerList = new ArrayList<Passenger>();		
		List<Passenger> errorPassengerList = new ArrayList<Passenger>();			
		List<Airport> errorAirportList = new ArrayList<Airport>();
        
        Stream<String> passenger;
        Stream<String> airport;
        Stream<String> errorPassenger;
       
        try {
        	
        	// read no error passenger file as passenger arraylist
	        passenger = Files.lines(Paths.get(Constants.NO_ERROR_PASSENGER_FILE_PATH));       	    	
    	
	        // remove duplicates
	        List<String> uniqueNoErrorPassengers = passenger
	    	    	.distinct()
	    	        .collect(Collectors.toList());
	        passenger.close();
	        
	        noErrorPassengerList = uniqueNoErrorPassengers.stream().map(l -> {
	    		String[] w = l.split(Constants.COMMA); 
	    		Passenger p = new Passenger(w[0],w[1],w[2],w[3],new Date(Long.parseLong(w[4]) * 1000),Integer.parseInt(w[5]));     
	    	    return p;
	    	})
	        .collect(Collectors.toList());
	    	
	    	
	        // read airport file as airport arraylist
			airport = Files.lines(Paths.get(Constants.AIRPORT_FILE_PATH));
			
			// remove duplicates
			List<String> uniqueAirports = airport
	    	    	.distinct()
	    	        .collect(Collectors.toList());
			airport.close();
			
			errorAirportList = uniqueAirports.stream().map(l -> {	    		
	    		Airport a = null;
	    		if(!l.isBlank()) {
	        		String[] w = l.split(Constants.COMMA); 
	        		a = new Airport(w[0],w[1],Float.parseFloat(w[2]),Float.parseFloat(w[3]));        			        		
	    		}
	    		return a;
	    	})
	        .collect(Collectors.toList());
			
	    	
			// read error passenger file as passenger arraylist
	    	errorPassenger = Files.lines(Paths.get(Constants.ERROR_PASSENGER_FILE_PATH));       	    	
	    	
	    	// remove duplicates
	    	List<String> uniqueErrorPassengers = errorPassenger
	    	.distinct()
	        .collect(Collectors.toList());
	    	errorPassenger.close();
	    	
	    	errorPassengerList = uniqueErrorPassengers.stream().map(l -> {
	    		String[] w = l.split(Constants.COMMA); 
	    		Passenger p = new Passenger(w[0],w[1],w[2],w[3],new Date(Long.parseLong(w[4]) * 1000),Integer.parseInt(w[5]));     
	    	    return p;
	    	})
	        .collect(Collectors.toList());
	    	
        
	    	System.out.println("noErrorPassengerList : "+noErrorPassengerList.get(0));
        } catch (IOException e) {
			System.out.println("Print : "+e);
		}
        
        
        Map m = new HashMap();
        m.put("errorAirport", errorAirportList);
        m.put("noErrorPassengerList", noErrorPassengerList);
        m.put("errorPassengerList", errorPassengerList);
        
        return m;
	}
	
	
	/**
	 *  Error detection and correction using Levenshtein Distance
	 */
	public static Map errorProcessing() {
		
		Map m = readFiles();  // read all the files
		
		String correctData = "";
		
		List<Airport> errorAirportList = (List<Airport>)m.get("errorAirport");		
		List<Passenger> noErrorPassengerList = (List<Passenger>)m.get("noErrorPassengerList");
		List<Passenger> errorPassengerList = (List<Passenger>)m.get("errorPassengerList");
		List<ErrorData> flightIdErrorList = new ArrayList<ErrorData>();
		List<ErrorData> passengerIdErrorList = new ArrayList<ErrorData>();
		List<ErrorData> airportCodeErrorList = new ArrayList<ErrorData>();
		List<ErrorData> invalidAirportCodes = new ArrayList<ErrorData>();
		List<Passenger> passengerDeleteList = new ArrayList<Passenger>();
		List<Airport> airportDeleteList = new ArrayList<Airport>();
		List<Passenger> uniquePassengerList =  new ArrayList<Passenger>();
		
		Stream<Passenger> passStream = noErrorPassengerList.stream();
		
		// remove the null and blank values
		errorPassengerList = errorPassengerList.stream()
				.filter(value -> value != null
				&& value.getDestAirportCode().trim().length() != 0
				&& value.getFlightId().trim().length() != 0
				&& value.getFromAirportCode().trim().length() != 0
				&& value.getPassengerId().trim().length() != 0)
				.collect(Collectors.toList());
		
		errorAirportList = errorAirportList.stream()
				.filter(value -> value != null 
				&& value.getAirportCode().trim().length() != 0
				&& value.getAirportName().trim().length() != 0
				&& value.getLatitude() != 0
				&& value.getLongitude() != 0)
				.collect(Collectors.toList());
		
		// passenger list error correction and detection
		for (Passenger passenger : errorPassengerList) {
			
			// capitalise string values
			passenger.setFlightId(passenger.getFlightId().toUpperCase());
			passenger.setPassengerId(passenger.getPassengerId().toUpperCase());
			passenger.setFromAirportCode(passenger.getFromAirportCode().toUpperCase());
			passenger.setDestAirportCode(passenger.getDestAirportCode().toUpperCase());		
			
			// error detection and correction of flight IDs
			if(!Pattern.matches(Constants.FLIGHT_ID_PATTERN, passenger.getFlightId())) {     // pattern match
				correctData = errorCorrection(noErrorPassengerList, null, passenger.getFlightId(), "flightId");
				if(!correctData.isEmpty()) {
					ErrorData e = new ErrorData();
					e.setCorrectData(correctData);
					e.setErrorData(passenger.getFlightId());
					flightIdErrorList.add(e);
					passenger.setFlightId(correctData);
				}else {													// else delete
					passengerDeleteList.add(passenger);
				}
			}
			
			// error detection and correction of passenger IDs
			if(!Pattern.matches(Constants.PASSENGER_ID_PATTERN, passenger.getPassengerId())) {  // pattern match
				correctData = errorCorrection(noErrorPassengerList, null, passenger.getPassengerId(), "passengerId");
				if(!correctData.isEmpty()) {
					ErrorData e = new ErrorData();
					e.setCorrectData(correctData);
					e.setErrorData(passenger.getPassengerId());
					passengerIdErrorList.add(e);
					passenger.setPassengerId(correctData);
				}else {                                             // else delete
					passengerDeleteList.add(passenger);
				}
			}
			
			// error detection and correction of from airport codes
			if(!Pattern.matches(Constants.AIRPORT_CODE_PATTERN, passenger.getFromAirportCode())) {		// pattern match	
				correctData = errorCorrection(null, errorAirportList, passenger.getFromAirportCode(), "");
				if(!correctData.isEmpty()) {
					ErrorData e = new ErrorData();
					e.setCorrectData(correctData);
					e.setErrorData(passenger.getFromAirportCode());
					airportCodeErrorList.add(e);
					passenger.setFromAirportCode(correctData);
				}else {
					passengerDeleteList.add(passenger);				// else delete
				}
			}else {													
				Optional<Airport> fromAirport = errorAirportList.stream().filter(t -> t.getAirportCode().equalsIgnoreCase(passenger.getFromAirportCode())).findAny();

				if(fromAirport.isEmpty()) {
					ErrorData e = new ErrorData();
					e.setErrorData(passenger.getFromAirportCode());
					invalidAirportCodes.add(e);                     // generate list of invalid airport codes
					passengerDeleteList.add(passenger);
				}
			}
			
			// error detection and correction of destination airport codes
			if(!Pattern.matches(Constants.AIRPORT_CODE_PATTERN, passenger.getDestAirportCode())) {		// pattern match	
				correctData = errorCorrection(null, errorAirportList, passenger.getDestAirportCode(), "");
				if(!correctData.isEmpty()) {
					ErrorData e = new ErrorData();
					e.setCorrectData(correctData);
					e.setErrorData(passenger.getDestAirportCode());
					airportCodeErrorList.add(e);
					passenger.setDestAirportCode(correctData);
				}else {											 // else delete
					passengerDeleteList.add(passenger);
				}
			}else {
				Optional<Airport> destAirport = errorAirportList.stream().filter(t -> t.getAirportCode().equalsIgnoreCase(passenger.getDestAirportCode())).findAny();
				if(destAirport.isEmpty()) {
					ErrorData e = new ErrorData();
					e.setErrorData(passenger.getDestAirportCode());
					invalidAirportCodes.add(e);					// generate list of invalid airport codes
					passengerDeleteList.add(passenger);
				}
			}
			
		}
		
		for (Airport airport : errorAirportList) {
			airport.setAirportCode(airport.getAirportCode().toUpperCase());

			if(!Pattern.matches(Constants.AIRPORT_CODE_PATTERN, String.valueOf(airport.getAirportCode()))) {// pattern match
				airportDeleteList.add(airport);                 // delete airport
			}
			
		}
		
		// delete the passenger list and remove duplicates after the error correction
		for (Passenger passenger1 : errorPassengerList) {
			boolean isEqual = false;
			boolean delete = false;
			
			for(Passenger passenger2 : passengerDeleteList) {
				if(passenger1.toString().equalsIgnoreCase(passenger2.toString())) {
					delete = true;
				}
			}
			if(delete) {
				continue;
			}
			if(!uniquePassengerList.isEmpty()) {
				for (Passenger passenger: uniquePassengerList) {
					if(passenger1.toString().equalsIgnoreCase(passenger.toString())) {
						isEqual = true;
					}
				}
			}else{
				uniquePassengerList.add(passenger1);
				continue;
			}
			
			if(!isEqual) {
				uniquePassengerList.add(passenger1);
			}
		}
		
		
		Map output = new HashMap();
		output.put("flightIdError", flightIdErrorList);
		output.put("passengerIdError", passengerIdErrorList);
		output.put("airportCodeError", airportCodeErrorList);
		output.put("passengerList", uniquePassengerList);
		output.put("errorAirportList", errorAirportList);
		output.put("invalidAirportCodes", invalidAirportCodes);
			
		return output;
		
	}
	
	
	/**
	 * Calculate the Levenshtein Distance for error correction
	 * 
	 * @param passengerList
	 * @param airportList
	 * @param errorData
	 * @param dataType
	 * @return
	 */
	public static String errorCorrection(List<Passenger> passengerList, List<Airport> airportList, String errorData, String dataType) {
		int minD = 9999;
		String correctData = "";
		int d = 0 ;

		// calculate the minimum distance for airport code
		if(airportList != null) {
		    for (Airport a : airportList) {
		        d = LevenshteinDistance.getDefaultInstance().apply(a.getAirportCode(), errorData);	        
		        if(minD > d) {
		        	minD = d;
		        	correctData = a.getAirportCode();
		        }
		    }
		}
		if(passengerList != null) {
			for (Passenger p : passengerList) {
				
				// calculate the minimum distance for passenger ID
				if(dataType.equalsIgnoreCase("passengerId")) {
					d = LevenshteinDistance.getDefaultInstance().apply(p.getPassengerId(), errorData);
					if(minD > d) {
			        	minD = d;
			        	correctData = p.getPassengerId();
			        }
				}
				
				// calculate the minimum distance for flight ID
				else if(dataType.equalsIgnoreCase("flightId")) {
					d = LevenshteinDistance.getDefaultInstance().apply(p.getFlightId(), errorData);
					if(minD > d) {
			        	minD = d;
			        	correctData = p.getFlightId();
			        }
				}
		    }
		}
		
		// return the data with the minimum distance
		return correctData;
	}
	
}
