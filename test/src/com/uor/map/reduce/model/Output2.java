package com.uor.map.reduce.model;

import java.util.Date;
import java.util.List;

public class Output2 {

	private String flightId;
	private int passengerCount;
	private String fromAirportName;
	private String destAirportName;
	private Date departureTime;
	private int totalFlightTime;
	private String arrivalTime;
	
		
	public Date getDepartureTime() {
		return departureTime;
	}
	public void setDepartureTime(Date departureTime) {
		this.departureTime = departureTime;
	}
	public int getTotalFlightTime() {
		return totalFlightTime;
	}
	public void setTotalFlightTime(int totalFlightTime) {
		this.totalFlightTime = totalFlightTime;
	}
	public String getArrivalTime() {
		return arrivalTime;
	}
	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	public String getFromAirportName() {
		return fromAirportName;
	}
	public void setFromAirportName(String fromAirportName) {
		this.fromAirportName = fromAirportName;
	}
	public String getDestAirportName() {
		return destAirportName;
	}
	public void setDestAirportName(String destAirportName) {
		this.destAirportName = destAirportName;
	}
	public String getFlightId() {
		return flightId;
	}
	public void setFlightId(String flightId) {
		this.flightId = flightId;
	}
	public int getPassengerCount() {
		return passengerCount;
	}
	public void setPassengerCount(int passengerCount) {
		this.passengerCount = passengerCount;
	}
	@Override
	public String toString() {
		return "Output2 [flightId=" + flightId + ", passengerCount=" + passengerCount + ", fromAirportName="
				+ fromAirportName + ", destAirportName=" + destAirportName + ", departureTime=" + departureTime
				+ ", totalFlightTime=" + totalFlightTime + ", arrivalTime=" + arrivalTime + "]";
	}
	
	
	
	
	
	
	
}
