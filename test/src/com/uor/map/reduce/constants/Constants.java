package com.uor.map.reduce.constants;

public class Constants {

	public static final String COMMA = ",";
	
	// file names
	public static final String OUTPUT_FILE_NAME = "map-reduce-output.pdf";
	
	// file paths
	public static final String JASPER__REPORT_TEMPLATE_PATH = "C:\\Users\\Shreya Desai\\Desktop\\Study\\Cloud_Computing\\assignment\\git-project\\map-reduce-prototype\\test\\src\\com\\uor\\map\\reduce\\jasper\\config\\mapreduce_output.jrxml";
	public static final String OUTPUT_FILE_PATH = "C:\\Users\\Shreya Desai\\Desktop\\Study\\Cloud_Computing\\";
	public static final String NO_ERROR_PASSENGER_FILE_PATH = "C:\\Users\\Shreya Desai\\Desktop\\Study\\Cloud_Computing\\assignment\\git-project\\map-reduce-prototype\\test\\src\\data\\AComp_Passenger_data_no_error.csv";
	public static final String AIRPORT_FILE_PATH = "C:\\Users\\Shreya Desai\\Desktop\\Study\\Cloud Computing\\assignment\\git-project\\map-reduce-prototype\\test\\src\\data\\Top30_airports_LatLong(1).csv";
	public static final String ERROR_PASSENGER_FILE_PATH = "C:\\Users\\Shreya Desai\\Desktop\\Study\\Cloud Computing\\assignment\\git-project\\map-reduce-prototype\\test\\src\\data\\Top30_airports_LatLong(1).csv";
	
	// Regex patterns for error detection in the data
	public static final String PASSENGER_ID_PATTERN = "[A-Z]{3}[0-9]{4}[A-Z]{2}[0-9]{1}";
	public static final String FLIGHT_ID_PATTERN = "[A-Z]{3}[0-9]{4}[A-Z]{1}";
	public static final String AIRPORT_CODE_PATTERN = "[A-Z]{3}";
	public static final String FLIGHT_TIME_PATTERN = "[0-9]{4}";
}