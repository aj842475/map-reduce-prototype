package com.uor.map.reduce.core;


import java.io.IOException;


/**
 * Mapper class
 *
 */
public abstract class Mapper {
	
	// The input file for this mapper to process
    public Object inputObj1;
    public Object inputObj2;
    public Object outputObj;
    

    // Default constructor
    public Mapper() {}

    // Set the input files for the given instance
    public void setFile(Object obj1, Object obj2) {
    	if(obj1 != null) {
    		this.inputObj1 = obj1;
    	}
    	if(obj2 != null) {
    		this.inputObj2 = obj2;
    	}
        
    }

    // Execute the map function for the data
    public void run() throws IOException {
    	map(inputObj1,inputObj2);
    }

    // Abstract map function to be overwritten by objective-specific class
    public abstract void map(Object obj1, Object obj2);

}
