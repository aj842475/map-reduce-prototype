package com.uor.map.reduce.model;

import java.util.Date;
import java.util.List;

public class ModelObj2 {

	private List<Passenger> passengerList;
	private int passengerCount;
	
	
	public List<Passenger> getPassengerList() {
		return passengerList;
	}
	public void setPassengerList(List<Passenger> passengerList) {
		this.passengerList = passengerList;
	}
	public int getPassengerCount() {
		return passengerCount;
	}
	
	public void setPassengerCount(int passengerCount) {
		this.passengerCount = passengerCount;
	}
	
	@Override
	public String toString() {
		return "ModelObj2 [passengerList=" + passengerList + ", passengerCount=" + passengerCount + "]";
	}	
	
	
}
