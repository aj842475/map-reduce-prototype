package com.uor.map.reduce.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.uor.map.reduce.core.Job;
import com.uor.map.reduce.core.Mapper;
import com.uor.map.reduce.core.Reducer;
import com.uor.map.reduce.model.Airport;
import com.uor.map.reduce.model.Passenger;

/**
 * Executes objective 3 
 *
 */
public class Obj3 {

	/**
	 * Executes map phase
	 *
	 */
	public static class MapperObj3 extends Mapper {
		
		public void map(Object obj1, Object obj2) { 
			
			List<Passenger> p = (List<Passenger>)obj1;
			List<Airport> a = (List<Airport>)obj2;
			
			Map map = new HashMap();
			map = Job.getMapOutput();       // get map output object
			
			for(int i=0;i<p.size();i++) {
				
				String key = p.get(i).getPassengerId();     // passenger ID as key
				String fromAirportCode = p.get(i).getFromAirportCode();
				String destAirportCode = p.get(i).getDestAirportCode();
				
				// fetch the latitude and longitude of from and destination airports
				for (Iterator iterator = a.iterator(); iterator.hasNext();) {
					Airport airport = (Airport) iterator.next();						
					if(airport != null) {
						Optional<Airport> fromAirport = a.stream().filter(t -> t.getAirportCode().equalsIgnoreCase(fromAirportCode)).findAny();
						Optional<Airport> destAirport = a.stream().filter(t -> t.getAirportCode().equalsIgnoreCase(destAirportCode)).findAny();
						if(!fromAirport.isEmpty() && !destAirport.isEmpty()) {
							// calculate nautical miles
							p.get(i).setNauticalMiles(fromAirport.get().getLatitude(), fromAirport.get().getLongitude(), destAirport.get().getLatitude(), destAirport.get().getLongitude());
						}
					}					
				}
				
				List values = new ArrayList<>();					
		        if(map.containsKey(key)) {
		            values = (List) map.get(key);
		            values.add(p.get(i));
		            map.replace(key, values);  // update value of key
		        }else {
		        	if(values != null) {
			        	values.add(p.get(i));
			        }
		        	map.put(key, values);      
		        }
			}
			Job.setMapOutput(map);        // set map output
		}
    }

	/**
	 * Executes reducer phase
	 *
	 */
	public static class ReducerObj3 extends Reducer {
		
    	public void reduce(Object key, Object values) {

        	Map m = Job.getReducerOutput();   //get reducer output
        	double totalMiles = 0;
    		List vals = new ArrayList();
    		List<Passenger> pList = new ArrayList<Passenger>();
        	vals = (ArrayList)values;
        	
            for (Object value : vals) {
            	Passenger p = (Passenger)value;
            	if(p.getNauticalMiles() != null) {
            		totalMiles += p.getNauticalMiles();  // calculate total nautical miles
            	}           	
            }
            if(!m.isEmpty()) {
            	Iterator it = m.entrySet().iterator();
            	while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry)it.next();
                    
                    // calculate the highest miles    
                	if(!pair.getKey().equals(key) && (double)pair.getValue() < totalMiles) {        		
                		m.remove(pair.getKey());
                		m.put(key, totalMiles);                	
                	}
                }          	
            }else {
            	m.put(key, totalMiles);
            }            
            Job.setReducerOutput(m);		// set reducer output
    	}
    }
}
