package com.uor.map.reduce.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.uor.map.reduce.constants.Constants;
import com.uor.map.reduce.model.Airport;
import com.uor.map.reduce.model.Passenger;



public class Config {

    private List<Passenger> passengerList;

	private List<Airport> airportList;
    
    // Classes to implement job-specific map and reduce functions
    private Class mapper, reducer, combiner;
    
    private int num_core;
    
    public List  getPassengerList() {
		return passengerList;
	}

	public void setPassengerList(List passengerList) {
		this.passengerList = passengerList;
	}
	
	public List<Airport> getAirportList() {
		return airportList;
	}

	public void setAirportList(List<Airport> airportList) {
		this.airportList = airportList;
	}
	
	
    // Constructor
    public Config(Map processedFiles, Class mapper, Class reducer, Class combiner) {
        this.mapper = mapper;
        this.reducer = reducer;
        this.combiner = combiner;
        this.airportList = (List<Airport>)processedFiles.get("errorAirportList");
        this.passengerList = (List<Passenger>)processedFiles.get("passengerList");
        
    }

    	


    

    // Using reflection get an instance of the mapper operating on a specified file
	/*
	 * protected Mapper getMapperInstance(File file) throws Exception { Mapper
	 * mapper = (Mapper) this.mapper.getConstructor().newInstance();
	 * mapper.setFile(file); return mapper; }
	 */
    protected Mapper getMapperInstance() throws Exception {
        Mapper mapper = (Mapper) this.mapper.getConstructor().newInstance();
//        mapper.setFile(file);
        return mapper;
    }
    // Using reflection get an instance of the reducer operating on a chunk of the intermediate results
	/*
	 * protected Reducer getReducerInstance(Map results) throws Exception { Reducer
	 * reducer = (Reducer) this.reducer.getConstructor().newInstance();
	 * reducer.setRecords(results); return reducer; }
	 */
    protected Reducer getReducerInstance() throws Exception {
        Reducer reducer = (Reducer) this.reducer.getConstructor().newInstance();
       // reducer.setRecords(results);
        return reducer;
    }
    
    protected Combiner getCombinerInstance() throws Exception {
    	Combiner combiner = null;
    	if(this.combiner != null) {
    		combiner = (Combiner) this.combiner.getConstructor().newInstance();            
    	}
        
        return combiner;
    }
    
    protected int getNumberCores() throws Exception {
        return Runtime.getRuntime().availableProcessors();
    }
}
