package com.uor.map.reduce.model;

public class Output1 {
	
	private String airportCode;
	private String airportName;
	private int flightCount;
	
	public String getAirportCode() {
		return airportCode;
	}
	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}
	public String getAirportName() {
		return airportName;
	}
	public void setAirportName(String airportName) {
		this.airportName = airportName;
	}
	public int getFlightCount() {
		return flightCount;
	}
	public void setFlightCount(int flightCount) {
		this.flightCount = flightCount;
	}
	@Override
	public String toString() {
		return "Output1 [airportCode=" + airportCode + ", airportName=" + airportName + ", flightCount=" + flightCount
				+ "]";
	}
	
	
}
