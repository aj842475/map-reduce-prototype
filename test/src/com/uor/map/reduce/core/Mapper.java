package com.uor.map.reduce.core;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class Mapper {
	// The input file for this mapper to process
    public volatile Object inputObj1;
    public volatile Object inputObj2;
    public Object outputObj;
    

    // Default constructor
    public Mapper() {}

    // Set the input file for the given instance
    public void setFile(Object obj1, Object obj2) {
    	if(obj1 != null) {
    		this.inputObj1 = obj1;
    	}
    	if(obj2 != null) {
    		this.inputObj2 = obj2;
    	}
        
    }

    // Execute the map function for each line of the provided file
    synchronized public void run() throws IOException {
        // Read the file
		/*
		 * Iterator<String> records = Config.read(); // Call map() for each line
		 * while(records.hasNext())
		
            map(records.next()); */
    	map(inputObj1,inputObj2);
    }

    // Abstract map function to be overwritten by objective-specific class
//    public abstract void map(String value);
    public abstract void map(Object obj1, Object obj2);

    // Adds values to a list determined by a key
    // Map<KEY, List<VALUES>>
    public void EmitIntermediate(Object key, Object value) {
        // Get the existing values linked to the observed key else create a new map entry with an empty list
        List values = new ArrayList<>();
        if(Job.mapOutput.containsKey(key)) {
            values = (List) Job.mapOutput.get(key);
        } else {
//        	System.out.println("else===========");
//            values = new ArrayList<>();
            Job.mapOutput.put(key, values);
        }
        // Add the new value to the list
        if(values != null) {
        	values.add(value);
        }
        
    }
}
