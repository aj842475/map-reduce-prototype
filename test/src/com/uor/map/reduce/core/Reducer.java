package com.uor.map.reduce.core;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public abstract class Reducer {
	// Intermediate records for this reducer instance to process
    public volatile Map.Entry records;

    // Default constructor
    public Reducer() {}

    // Setters
    public void setRecords(Map.Entry records) {
        this.records = records;
    }

    // Execute the reduce function for each key-value pair in the intermediate results output by the mapper
    synchronized public void run() {	

    	Map.Entry m = (Map.Entry)records;

    	
    	reduce(m.getKey(), m.getValue());			     		
		
    }

    // Abstract reduce function to the overwritten by objective-specific class
//    public abstract void reduce(Object key, List values);
    public abstract void reduce(Object key, Object values);

    // Simply replace the intermediate and final result for each key
    // Map <KEY, List<VALUES>> -> Map <KEY, VALUE>
    public void Emit(Object key, Object value) {
        Job.reducerOutput.put(key, value);
    }
}
