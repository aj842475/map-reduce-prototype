package com.uor.map.reduce.core;


import java.util.List;
import java.util.Map;

import com.uor.map.reduce.model.Airport;
import com.uor.map.reduce.model.Passenger;



/**
 * Saves job configuration
 *
 */
public class Config {

	// input buffer
    private List<Passenger> passengerList;
    
	private List<Airport> airportList;
    
    // Classes to implement job-specific map and reduce functions
    private Class mapper, reducer;
    
    
    public List  getPassengerList() {
		return passengerList;
	}

	public void setPassengerList(List passengerList) {
		this.passengerList = passengerList;
	}
	
	public List<Airport> getAirportList() {
		return airportList;
	}

	public void setAirportList(List<Airport> airportList) {
		this.airportList = airportList;
	}
	
	
    // initialize the objects
    public Config(Map processedFiles, Class mapper, Class reducer) {
        this.mapper = mapper;
        this.reducer = reducer;
        this.airportList = (List<Airport>)processedFiles.get("errorAirportList");
        this.passengerList = (List<Passenger>)processedFiles.get("passengerList");
        
    }

    // Using reflection get an instance of the mapper operating on a specified file
   protected Mapper getMapperInstance() throws Exception {
        Mapper mapper = (Mapper) this.mapper.getConstructor().newInstance();
        return mapper;
    }
   
    // Using reflection get an instance of the reducer operating on a chunk of the intermediate results	 
    protected Reducer getReducerInstance() throws Exception {
        Reducer reducer = (Reducer) this.reducer.getConstructor().newInstance();
        return reducer;
    }
    
}
