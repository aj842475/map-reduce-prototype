package com.uor.map.reduce.model;

import java.util.List;

public class ModelObj1 {

	private List flights;
	private String airportName;
	private int flightCount;
	
	
	
	public List getFlights() {
		return flights;
	}
	public void setFlights(List flights) {
		this.flights = flights;
	}
	public int getFlightCount() {
		return flightCount;
	}
	public void setFlightCount(int flightCount) {
		this.flightCount = flightCount;
	}
	public String getAirportName() {
		return airportName;
	}
	public void setAirportName(String airportName) {
		this.airportName = airportName;
	}
	
	@Override
	public String toString() {
		return "ModelObj1 [flights=" + flights + ", airportName=" + airportName + ", flightCount=" + flightCount + "]";
	}
	
	
	
}	