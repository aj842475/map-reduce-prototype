package com.uor.map.reduce.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import com.uor.map.reduce.constants.Constants;
import com.uor.map.reduce.controller.Obj1.MapperObj1;
import com.uor.map.reduce.controller.Obj1.ReducerObj1;
import com.uor.map.reduce.controller.Obj2.MapperObj2;
import com.uor.map.reduce.controller.Obj2.ReducerObj2;
import com.uor.map.reduce.controller.Obj3.MapperObj3;
import com.uor.map.reduce.controller.Obj3.ReducerObj3;
import com.uor.map.reduce.core.Config;
import com.uor.map.reduce.core.Job;
import com.uor.map.reduce.facade.FileProcessingFacade;
import com.uor.map.reduce.model.Airport;
import com.uor.map.reduce.model.ErrorData;
import com.uor.map.reduce.model.ModelObj1;
import com.uor.map.reduce.model.ModelObj2;
import com.uor.map.reduce.model.Output1;
import com.uor.map.reduce.model.Output2;
import com.uor.map.reduce.model.Passenger;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;

public class MapReduceController {

	public static void main(String[] args) throws Exception {
		
		Map processedFiles = FileProcessingFacade.errorProcessing();
		
        Config config1 = new Config(processedFiles, MapperObj1.class, ReducerObj1.class, null);
        Job job1 = new Job(config1);
        job1.run();
//        System.out.println("job1.getReducerOutput() : "+job1.getReducerOutput());
		Map job1Op =  job1.getReducerOutput();
		
		Config config2 = new Config(processedFiles, MapperObj2.class, ReducerObj2.class, null);
		Job job2 = new Job(config2); 
		job2.run();
		Map job2Op =  job2.getReducerOutput();
//		System.out.println("job2.gAetReducerOutput() : "+job2.getReducerOutput().size()); 

		
		Config config3 = new Config(processedFiles, MapperObj3.class, ReducerObj3.class, null);
		Job job3 = new Job(config3); 
		job3.run();
		Map job3Op =  job3.getReducerOutput();
		System.out.println("job3.getReducerOutput() : "+job3Op);
		 

//		System.out.println("job2.getReducerOutput() : "+job2.getReducerOutput().get("XXQ4064B"));

		List<Airport> airportList = (List<Airport>)processedFiles.get("errorAirportList");
		List<Output2> output2List = new ArrayList<Output2>();
		List<Output1> output1List = new ArrayList<Output1>();
		
		Iterator it1 = job1Op.entrySet().iterator();
		  
		while (it1.hasNext()) {
			Map.Entry pair = (Map.Entry)it1.next();
		    Output1 o = new Output1();
		    ModelObj1 mo = (ModelObj1)pair.getValue();
		    o.setAirportCode((String)pair.getKey());
		    o.setAirportName(mo.getAirportName());
		    o.setFlightCount(mo.getFlightCount());
		    output1List.add(o);       		        
		}
		
		Comparator<Output1> compareByFlightCount = (Output1 o1, Output1 o2) -> 
		Integer.valueOf(o1.getFlightCount()).compareTo(Integer.valueOf(o2.getFlightCount() ) );
		Collections.sort(output1List, Collections.reverseOrder(compareByFlightCount));
		
		
		Iterator it2 = job2Op.entrySet().iterator();
		  
		while (it2.hasNext()) {
			Map.Entry pair = (Map.Entry)it2.next();
			ModelObj2 mo = (ModelObj2)pair.getValue();
		    Output2 o = new Output2();
		    o.setFlightId((String)pair.getKey());
		    o.setFromAirportName(mo.getPassengerList().get(0).getFromAirportCode());
		    o.setDestAirportName(mo.getPassengerList().get(0).getDestAirportCode());
		    o.setDepartureTime(mo.getPassengerList().get(0).getDepartureTime());
		    o.setArrivalTime(mo.getPassengerList().get(0).getArrivalTime());
		    o.setTotalFlightTime(mo.getPassengerList().get(0).getTotalFlightTime());
			o.setPassengerCount(mo.getPassengerCount());
			output2List.add(o);       		        
		}
		    	
		Comparator<Output2> compareByPassCount = (Output2 o1, Output2 o2) -> 
		Integer.valueOf(o1.getPassengerCount()).compareTo(Integer.valueOf(o2.getPassengerCount() ) );
		Collections.sort(output2List, Collections.reverseOrder(compareByPassCount));
				
		String highestAirMiles = "";
		Iterator it3 = job3Op.entrySet().iterator();
		
		while (it3.hasNext()) {
			Map.Entry pair = (Map.Entry)it3.next();
			String passengerId = (String)pair.getKey();
			double airMiles = (double)pair.getValue();
			System.out.println("passengerId : "+passengerId+" airMiles : "+airMiles);
			if(!passengerId.isEmpty()) {
				highestAirMiles = "The highest number of air miles "+airMiles+" are travelled by passenger ID "+passengerId;   
			}			     
		}
		
		System.out.println("highestAirMiles : "+highestAirMiles);
		try {

			Collection<Map<String, ?>> col = new ArrayList<>();
			col.add(job2.getReducerOutput());
						 
//          Give the path of report jrxml file path for complie.
			JasperReport jasperReport = JasperCompileManager.compileReport(Constants.JASPER__REPORT_TEMPLATE_PATH);
//	        System.out.println("job2.getReducerOutput() : "+job2.getReducerOutput());
			Map<String, Object> hm = new HashMap<String, Object>();
			hm.put("DatasourceObj1", new JRBeanCollectionDataSource(output1List,true));
			hm.put("DatasourceObj2", new JRBeanCollectionDataSource(output2List,true));
			hm.put("FlightIDError", new JRBeanCollectionDataSource((List<ErrorData>)processedFiles.get("flightIdError"),true));
			hm.put("PassengerIDError", new JRBeanCollectionDataSource((List<ErrorData>)processedFiles.get("passengerIdError"),true));
			hm.put("AirportCodeError", new JRBeanCollectionDataSource((List<ErrorData>)processedFiles.get("airportCodeError"),true));
			hm.put("HighestAirMiles", highestAirMiles);
	       	       
			//pass data HashMap with compiled jrxml file and a empty data source .
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, hm, new JREmptyDataSource());
			         
			// export file to pdf to your local system 
			JasperExportManager.exportReportToPdfFile(jasperPrint, Constants.OUTPUT_FILE_PATH + Constants.OUTPUT_FILE_NAME);
			System.out.println("Done---------------	8-------");
	          
	      } catch (Exception e) {
	          e.printStackTrace();
	          System.out.println(e);
	      }
    }
}
