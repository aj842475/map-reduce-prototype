package com.uor.map.reduce.model;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Passenger {
	
	private String passengerId;
	private String flightId;
	private String fromAirportCode;
	private String destAirportCode;
	private Date departureTime;
	private int totalFlightTime;
	private String arrivalTime;
	private Double nauticalMiles;
	
	public Passenger(String passengerId, String flightId, String fromAirportCode, String destAirportCode, 
			Date departureTime, int totalFlightTime){
		
		this.passengerId = passengerId;
		this.flightId = flightId;
		this.fromAirportCode = fromAirportCode;
		this.destAirportCode = destAirportCode;
		this.departureTime = departureTime;
		this.totalFlightTime = totalFlightTime;
	}
	
	public String getPassengerId() {
		return passengerId;
	}
	
	public void setPassengerId(String passengerId) {
		this.passengerId = passengerId;
	}
	
	public String getFlightId() {
		return flightId;
	}
	
	public void setFlightId(String flightId) {
		this.flightId = flightId;
	}
	
	public String getFromAirportCode() {
		return fromAirportCode;
	}
	
	public void setFromAirportCode(String fromAirportCode) {
		this.fromAirportCode = fromAirportCode;
	}
	
	public String getDestAirportCode() {
		return destAirportCode;
	}
	
	public void setDestAirportCode(String destAirportCode) {
		this.destAirportCode = destAirportCode;
	}
	
	public Date getDepartureTime() {
		return departureTime;
	}
	
	public void setDepartureTime(Date departureTime) {
		this.departureTime = departureTime;
	}
	
	public int getTotalFlightTime() {
		return totalFlightTime;
	}
	
	public void setTotalFlightTime(int totalFlightTime) {
		this.totalFlightTime = totalFlightTime;
	}
	
	public void setArrivalTime(Date departureTime, int totalFlightTime) {
		
		long ONE_MINUTE_IN_MILLIS = 60000;		
		long t= departureTime.getTime();
		Date afterAddingFlightTime = new Date(t + (totalFlightTime * ONE_MINUTE_IN_MILLIS));
		SimpleDateFormat format = new SimpleDateFormat("hh:mm:ss");
		String dateString = format.format(afterAddingFlightTime);
		
		this.arrivalTime = dateString;
	}
	
	public String getArrivalTime() {
		return arrivalTime;
	}

	public void setNauticalMiles(double lat1, double lon1, double lat2, double lon2) {
		
		double dist;
		if ((lat1 == lat2) && (lon1 == lon2)) {
			dist = 0;
		}
		else {
			double theta = lon1 - lon2;
			dist = Math.sin(Math.toRadians(lat1)) * Math.sin(Math.toRadians(lat2)) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.cos(Math.toRadians(theta));
			dist = Math.acos(dist);
			dist = Math.toDegrees(dist);
			dist = dist * 60 * 1.1515;
			dist = dist * 0.8684;
		}
		this.nauticalMiles = dist;
	}
	
	public Double getNauticalMiles() {
		return nauticalMiles;
	}

	@Override
	public String toString() {
		return "Passenger [passengerId=" + passengerId + ", flightId=" + flightId + ", fromAirportCode="
				+ fromAirportCode + ", destAirportCode=" + destAirportCode + ", departureTime=" + departureTime
				+ ", totalFlightTime=" + totalFlightTime + ", arrivalTime=" + arrivalTime + "]";
	}
	
	
}
