package com.uor.map.reduce.model;



public class Airport {
	
	private String airportName;
	private String airportCode;
	private Float latitude;
	private Float longitude;
	
	public Airport(String airportName, String airportCode, Float latitude, Float longitude){
		
		this.airportName = airportName;
		this.airportCode = airportCode;
		this.latitude = latitude;
		this.longitude = longitude;
	}
	
	public String getAirportName() {
		return airportName;
	}
	public void setAirportName(String airportName) {
		this.airportName = airportName;
	}
	public String getAirportCode() {
		return airportCode;
	}
	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}
	public Float getLatitude() {
		return latitude;
	}
	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}
	public Float getLongitude() {
		return longitude;
	}
	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}

	@Override
	public String toString() {
		return "Airport [airportName=" + airportName + ", airportCode=" + airportCode + ", latitude=" + latitude
				+ ", longitude=" + longitude + "]";
	}
	
	
}
