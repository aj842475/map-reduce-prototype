package com.uor.map.reduce.core;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.uor.map.reduce.model.Airport;
import com.uor.map.reduce.model.Passenger;

public class Job {
	// Job configuration
    private Config config;
    
    private ThreadPoolExecutor executor;

    // Global object to store intermediate and final results
    protected static Map mapOutput;
    
    protected static Map combinerOutput;
    
    protected static Map reducerOutput;

    

    synchronized public static Map getMapOutput() {
		return mapOutput;
	}

    synchronized public static void setMapOutput(Map mapOutput) {
		Job.mapOutput = mapOutput;
	}

    synchronized public static Map getCombinerOutput() {
		return combinerOutput;
	}

    synchronized public static void setCombinerOutput(Map combinerOutput) {
		Job.combinerOutput = combinerOutput;
	}

    synchronized public static Map getReducerOutput() {
		return reducerOutput;
	}

    synchronized public static void setReducerOutput(Map reducerOutput) {
		Job.reducerOutput = reducerOutput;
	}

	// Constructor
    public Job(Config config) {
        this.config = config;
//        this.executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(100);
        this.executor = new ThreadPoolExecutor(100, 100, 0L, TimeUnit.MILLISECONDS, new ArrayBlockingQueue<Runnable>(15));
    }

    // Run the job given the provided configuration
    public void run() throws Exception {
        // Initialise the map to store intermediate results
    	setReducerOutput(Collections.synchronizedMap(new HashMap()));
    	setCombinerOutput(Collections.synchronizedMap(new HashMap()));
    	setMapOutput(Collections.synchronizedMap(new HashMap()));
//    	Job.mapOutput = Collections.synchronizedMap(new HashMap());
//    	this.combinerOutput = Collections.synchronizedMap(new HashMap());
//    	this.reducerOutput = Collections.synchronizedMap(new HashMap());
    	
        
        // Execute the map and reduce phases in sequence
        map();
//        System.out.println("Map output : "+getMapOutput());

//        if(config.getCombinerInstance() != null) {
//        	combiner();
//        	System.out.println("Combiner output : "+combinerOutput);
//        }
//		synchronized (mapOutput) {
//			Map temp = this.mapOutput; Iterator it = temp.entrySet().iterator(); while
//				  (it.hasNext()) { Map.Entry pair = (Map.Entry)it.next();
//				  System.out.println(pair.getKey()+" "+pair.getValue()); ////
//				  pair.getValue(); 
//				  it.remove(); 
//			}
//		}
		  // avoids a ConcurrentModificationException }
		 
        reduce();
//        System.out.println("Reducer output : "+getReducerOutput());
        // Output the results to the console
        
    }

    // Map each provided file using an instance of the mapper specified by the job configuration
    private void map() throws Exception {
		/*
		 * for(File file : config.getFiles()) { Mapper mapper =
		 * config.getMapperInstance(file); mapper.run(); }
		 */
    	List<Passenger> passengerList = new ArrayList<Passenger>();
    	List<Airport> airportList = new ArrayList<Airport>();
    	
    	passengerList = config.getPassengerList();
    	airportList = config.getAirportList();
    	
    	Mapper mapper = config.getMapperInstance();   
    	mapper.setFile(passengerList, airportList);
    	mapper.run();
        System.out.println("***********************Completed Map Phase**********************");

        
    }

    private void combiner() throws Exception {
        System.out.println("***********************Completed Combiner Phase**********************");
    }
    
    // Reduce the intermediate results output by the map phase using an instance of the reducer specified by the job configuration
    private void reduce() throws Exception {
    	
    	Map m = new HashMap();
    	m = getMapOutput();
//    	System.out.println("m : "+m);
    	Reducer reducer = config.getReducerInstance();
    	Iterator it = m.entrySet().iterator();
    	while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
//            System.out.println("key : "+pair.getKey()+" "+"value : "+pair.getValue());
            reducer.setRecords(pair);      
            reducer.run();
        }
    	
	
        System.out.println("***********************Completed Reducer Phase**********************");
    }
    
    
}
