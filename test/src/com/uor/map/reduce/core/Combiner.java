package com.uor.map.reduce.core;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public abstract class Combiner {
	protected Map records;

    // Default constructor
    public Combiner() {}

    // Setters
    public void setRecords(Map records) {
        this.records = records;
    }
    
    public void run() throws IOException {
    	Iterator iterator = ((Map)records).entrySet().iterator(); while(iterator.hasNext())
		{ 
			Map.Entry<Object, List<Object>> entry = (Map.Entry) iterator.next(); //
			combiner(entry.getKey(), entry.getValue());		  
		}
    	
    }
    
    public abstract void combiner(Object key, Object value) ;
    	
   

    // Simply replace the intermediate and final result for each key
    // Map <KEY, List<VALUES>> -> Map <KEY, VALUE>
    public void Emit(Object key, Object value) {
        Job.combinerOutput.put(key, value);
    }
}
