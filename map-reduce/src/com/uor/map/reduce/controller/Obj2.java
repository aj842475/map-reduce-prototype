package com.uor.map.reduce.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.uor.map.reduce.core.Job;
import com.uor.map.reduce.core.Mapper;
import com.uor.map.reduce.core.Reducer;
import com.uor.map.reduce.model.ModelObj2;
import com.uor.map.reduce.model.Passenger;

/**
 * Executes objective 2
 *
 */
public class Obj2 {
	
	/**
	 * Executes map phase
	 *
	 */
	public static class MapperObj2 extends Mapper {
		
		public void map(Object obj1, Object obj2) { 
			
			List<Passenger> p = (List<Passenger>)obj1;
			Map map = new HashMap();
			map = Job.getMapOutput();       // get map output object
			
			for(int i=0;i<p.size();i++) {
				String key = p.get(i).getFlightId(); // flight ID as key
				List values = new ArrayList<>();					
		        if(map.containsKey(key)) {
		            values = (List) map.get(key);
		            values.add(p.get(i));
		            map.replace(key, values);  // update value of key
		        }else {
		        	if(values != null) {
			        	values.add(p.get(i));  
			        }
		        	map.put(key, values);     // set passenger list 
		        }
			}
			Job.setMapOutput(map);           // set map output
		}
    }
	
	
	/**
	 * Executes reducer phase
	 *
	 */
	public static class ReducerObj2 extends Reducer {
    	
		public void reduce(Object key, Object values) {

        	Map m = Job.getReducerOutput();  //get reducer output
        	ModelObj2 mo = new ModelObj2();
        	
    		List vals = new ArrayList();
    		List<Passenger> pList = new ArrayList<Passenger>();
        	vals = (ArrayList)values;
        	mo.setPassengerCount(vals.size());  // set passenger count
            for (Object value : vals) {
            	Passenger p = (Passenger)value;
            	p.setArrivalTime(p.getDepartureTime(), p.getTotalFlightTime());  // calculate arrival time
            	pList.add(p);
            }
            mo.setPassengerList(pList);
            m.put(key, mo);
            Job.setReducerOutput(m);         // set reducer output
    	}
    }
}
