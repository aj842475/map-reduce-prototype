package com.uor.map.reduce.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import com.uor.map.reduce.core.Job;
import com.uor.map.reduce.core.Mapper;
import com.uor.map.reduce.core.Reducer;
import com.uor.map.reduce.model.Airport;
import com.uor.map.reduce.model.ModelObj1;
import com.uor.map.reduce.model.Passenger;

/**
 * Executes objective 1 
 *
 */
public class Obj1 {
	
	/**
	 * Executes map phase
	 *
	 */
	public static class MapperObj1 extends Mapper {
		
		public void map(Object obj1, Object obj2) { 
			
			Map map = new HashMap();
			
			map = Job.getMapOutput();   // get map output object
			
			List<Passenger> p = (List<Passenger>)obj1;
			List<Airport> a = (List<Airport>)obj2;
					
			for(int i=0;i<p.size();i++) {				
				String key = p.get(i).getFromAirportCode(); // from airport code as key
				Airport filteredAirport = a
						.stream()
						.filter(value -> value.getAirportCode().equals(key)) // fetch the Airport with the key
						.findAny()
						.orElse(null);			
				
				List<Integer> count = new ArrayList<Integer>();	
				
		        if(map.containsKey(key)) {
		        	ModelObj1 values = (ModelObj1) map.get(key);
		            values.getFlights().add(1);
		            map.replace(key, values);  // update value of key
		        }else {
		        	if(count != null) {
		        		count.add(1);
			        }
		        	ModelObj1 mo1 = new ModelObj1();
		        	mo1.setFlights(count);   // set count list for eg. [1,1,1]
		        	if(filteredAirport != null) {
		        		mo1.setAirportName(filteredAirport.getAirportName()); 
		        	}		        	
		        	map.put(key, mo1);  // set map output
		        }
			}
			
			// fetch unused airports and set the count as 0
			for (Iterator iterator = a.iterator(); iterator.hasNext();) {
				Airport airport = (Airport) iterator.next();
				
				if(airport != null) {
					if(!map.containsKey(airport.getAirportCode())) {
						List values = new ArrayList<>();
						values.add(0);
						ModelObj1 mo1 = new ModelObj1();
						mo1.setFlights(values);
						mo1.setAirportName(airport.getAirportName());
						map.put(airport.getAirportCode(), mo1);
					}
				}
			}
			Job.setMapOutput(map);    // add unused airports in map output
		}		
    }
	
	
	/**
	 * Executes reducer phase
	 *
	 */
    public static class ReducerObj1 extends Reducer {
    	
    	public void reduce(Object key, Object values) {
    		
        	int count = 0;
        	Map m = Job.getReducerOutput(); //get reducer output
        	
    		List vals = new ArrayList();
    		ModelObj1 m1 = (ModelObj1)values;
    		vals = m1.getFlights();
            for (Object value : vals) {
            	count += (int) value;      // calculate flight count
            }
            
            m1.setFlightCount(count);
            m.put(key, m1);
            Job.setReducerOutput(m);       // set reducer output
		}   
        	
    	
    }
}
