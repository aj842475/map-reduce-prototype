package com.uor.map.reduce.model;

public class ErrorData {
	
	private String correctData;
	private String errorData;
	
	public String getCorrectData() {
		return correctData;
	}
	public void setCorrectData(String correctData) {
		this.correctData = correctData;
	}
	public String getErrorData() {
		return errorData;
	}
	public void setErrorData(String errorData) {
		this.errorData = errorData;
	}
	@Override
	public String toString() {
		return "ErrorData [correctData=" + correctData + ", errorData=" + errorData + "]";
	}
	
	
}
