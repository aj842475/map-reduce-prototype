package com.uor.map.reduce.facade;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.uor.map.reduce.constants.Constants;
import com.uor.map.reduce.model.Airport;
import com.uor.map.reduce.model.ErrorData;
import com.uor.map.reduce.model.ModelObj1;
import com.uor.map.reduce.model.ModelObj2;
import com.uor.map.reduce.model.Output1;
import com.uor.map.reduce.model.Output2;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * Common utility
 *
 */
public class CommonFacade {

	/**
	 * Generates the output PDF report
	 * 
	 * @param processedFiles
	 * @param job1Op
	 * @param job2Op
	 * @param job3Op
	 */
	public static void exportOutputReportPDF(Map processedFiles, Map job1Op, Map job2Op, Map job3Op) {
		
		List<Airport> airportList = (List<Airport>)processedFiles.get("errorAirportList");
		List<Output2> output2List = new ArrayList<Output2>();
		List<Output1> output1List = new ArrayList<Output1>();
		List<Output1> unusedAirports = new ArrayList<Output1>();
		
		// create the objective 1 output for report
		Iterator it1 = job1Op.entrySet().iterator();
		  
		while (it1.hasNext()) {
			Map.Entry pair = (Map.Entry)it1.next();
			
		    Output1 o = new Output1();
		    ModelObj1 mo = (ModelObj1)pair.getValue();
		    o.setAirportCode((String)pair.getKey());
		    o.setAirportName(mo.getAirportName());
		    o.setFlightCount(mo.getFlightCount());
		    if(mo.getFlightCount() == 0) {
				unusedAirports.add(o);       // set flight count = 0 as unused airports
				it1.remove();
			}else {
				output1List.add(o); 
			}	          		        
		}
		
		// sort output in descending order by flight count
		Comparator<Output1> compareByFlightCount = (Output1 o1, Output1 o2) -> 
		Integer.valueOf(o1.getFlightCount()).compareTo(Integer.valueOf(o2.getFlightCount() ) );
		Collections.sort(output1List, Collections.reverseOrder(compareByFlightCount));	
		
		// create list of unused airports
		for (Output1 output1 : output1List) {
			if(output1.getFlightCount() == 0) {
				unusedAirports.add(output1);
			}
		}
		
		// create the objective 2 output for report
		Iterator it2 = job2Op.entrySet().iterator();
		  
		while (it2.hasNext()) {
			Map.Entry pair = (Map.Entry)it2.next();
			ModelObj2 mo = (ModelObj2)pair.getValue();
		    Output2 o = new Output2();
		    o.setFlightId((String)pair.getKey());
		    o.setFromAirportName(mo.getPassengerList().get(0).getFromAirportCode());
		    o.setDestAirportName(mo.getPassengerList().get(0).getDestAirportCode());
		    o.setDepartureTime(mo.getPassengerList().get(0).getDepartureTime());
		    o.setArrivalTime(mo.getPassengerList().get(0).getArrivalTime());
		    o.setTotalFlightTime(mo.getPassengerList().get(0).getTotalFlightTime());
			o.setPassengerCount(mo.getPassengerCount());
			output2List.add(o);       		        
		}
		
		// sort output in descending order by passenger count
		Comparator<Output2> compareByPassCount = (Output2 o1, Output2 o2) -> 
		Integer.valueOf(o1.getPassengerCount()).compareTo(Integer.valueOf(o2.getPassengerCount() ) );
		Collections.sort(output2List, Collections.reverseOrder(compareByPassCount));
				
		
		// create the objective 3 output for report
		String highestAirMiles = "";
		Iterator it3 = job3Op.entrySet().iterator();
		
		while (it3.hasNext()) {
			Map.Entry pair = (Map.Entry)it3.next();
			String passengerId = (String)pair.getKey();
			double airMiles = (double)pair.getValue();
			if(!passengerId.isEmpty()) {
				highestAirMiles = "The highest number of air miles "+airMiles+" are travelled by passenger ID "+passengerId;   
			}			     
		}
		
		try {
						 
			// Give the path of report jrxml file path for compile.
			JasperReport jasperReport = JasperCompileManager.compileReport(Constants.JASPER_REPORT_TEMPLATE_PATH);
			
			// set the parameters for the report
			Map<String, Object> hm = new HashMap<String, Object>();
			hm.put("DatasourceObj1", new JRBeanCollectionDataSource(output1List,true));
			hm.put("DatasourceObj2", new JRBeanCollectionDataSource(output2List,true));
			hm.put("FlightIDError", new JRBeanCollectionDataSource((List<ErrorData>)processedFiles.get("flightIdError"),true));
			hm.put("PassengerIDError", new JRBeanCollectionDataSource((List<ErrorData>)processedFiles.get("passengerIdError"),true));
			hm.put("AirportCodeError", new JRBeanCollectionDataSource((List<ErrorData>)processedFiles.get("airportCodeError"),true));
			hm.put("InvalidAirportCodes", new JRBeanCollectionDataSource((List<ErrorData>)processedFiles.get("invalidAirportCodes"),true));
			hm.put("UnusedAirports", new JRBeanCollectionDataSource(unusedAirports,true));
			hm.put("HighestAirMiles", highestAirMiles);
	       	       
			//pass data HashMap with compiled jrxml file and a empty data source .
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, hm, new JREmptyDataSource());
			         
			// export file to pdf to your local system 
			JasperExportManager.exportReportToPdfFile(jasperPrint, Constants.OUTPUT_FILE_PATH + Constants.OUTPUT_FILE_NAME);

	          
	      } catch (Exception e) {
	          e.printStackTrace();
	          System.out.println(e);
	      }
	}
	
}
