package com.uor.map.reduce.facade;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.text.similarity.LevenshteinDistance;

import com.uor.map.reduce.constants.Constants;
import com.uor.map.reduce.model.Airport;
import com.uor.map.reduce.model.ErrorData;
import com.uor.map.reduce.model.Passenger;



public class FileProcessingFacade {
	
	public static Map readFiles() {
		List<Passenger> noErrorPassengerList = new ArrayList<Passenger>();		
		
		List<Passenger> errorPassengerList = new ArrayList<Passenger>();			
		List<Airport> errorAirportList = new ArrayList<Airport>();
        
        Stream<String> passenger;
        Stream<String> airport;
        Stream<String> errorPassenger;
       
        try {
	        passenger = Files.lines(Paths.get(Constants.NO_ERROR_PASSENGER_FILE_PATH));       	    	
    	
	        List<String> uniqueNoErrorPassengers = passenger
	    	    	.distinct()
	    	        .collect(Collectors.toList());
	        passenger.close();
	        
	        noErrorPassengerList = uniqueNoErrorPassengers.stream().map(l -> {
	    		String[] w = l.split(Constants.COMMA); 
	    		Passenger p = new Passenger(w[0],w[1],w[2],w[3],new Date(Long.parseLong(w[4]) * 1000),Integer.parseInt(w[5]));     
	    	    return p;
	    	})
	        .collect(Collectors.toList());
	    	
	    	
			airport = Files.lines(Paths.get(Constants.AIRPORT_FILE_PATH));
			
			List<String> uniqueAirports = airport
	    	    	.distinct()
	    	        .collect(Collectors.toList());
			airport.close();
			
			errorAirportList = uniqueAirports.stream().map(l -> {	    		
	    		Airport a = null;
	    		if(!l.isBlank()) {
	        		String[] w = l.split(Constants.COMMA); 
	        		a = new Airport(w[0],w[1],Float.parseFloat(w[2]),Float.parseFloat(w[3]));        			        		
	    		}
	    		return a;
	    	})
	        .collect(Collectors.toList());
			
	    	
	    	
	    	errorPassenger = Files.lines(Paths.get(Constants.ERROR_PASSENGER_FILE_PATH));       	    	
	    	
	    	List<String> uniqueErrorPassengers = errorPassenger
	    	.distinct()
	        .collect(Collectors.toList());
	    	errorPassenger.close();
	    	
	    	errorPassengerList = uniqueErrorPassengers.stream().map(l -> {
	    		String[] w = l.split(Constants.COMMA); 
	    		Passenger p = new Passenger(w[0],w[1],w[2],w[3],new Date(Long.parseLong(w[4]) * 1000),Integer.parseInt(w[5]));     
	    	    return p;
	    	})
	        .collect(Collectors.toList());
	    	
        
	    	System.out.println("noErrorPassengerList : "+noErrorPassengerList.get(0));
        } catch (IOException e) {
			System.out.println("Print : "+e);
		}
        
        
        Map m = new HashMap();
        m.put("errorAirport", errorAirportList);
        m.put("noErrorPassengerList", noErrorPassengerList);
        m.put("errorPassengerList", errorPassengerList);
        
        return m;
	}
	
	public static Map errorProcessing() {
		
		Map m = readFiles();
		
		String correctData = "";
		
		List<Airport> errorAirportList = (List<Airport>)m.get("errorAirport");		
		List<Passenger> noErrorPassengerList = (List<Passenger>)m.get("noErrorPassengerList");
		List<Passenger> errorPassengerList = (List<Passenger>)m.get("errorPassengerList");
		List<ErrorData> flightIdErrorList = new ArrayList<ErrorData>();
		List<ErrorData> passengerIdErrorList = new ArrayList<ErrorData>();
		List<ErrorData> airportCodeErrorList = new ArrayList<ErrorData>();
		
		Stream<Passenger> passStream = noErrorPassengerList.stream();
		
		int minLenAirportName = 3;
		int maxLenAirportName = 10;
		
		
		List<Passenger> passengerDeleteList = new ArrayList<Passenger>();
		List<Airport> airportDeleteList = new ArrayList<Airport>();
		
		StringBuffer flightIdError = new StringBuffer();
		StringBuffer passengerIdError = new StringBuffer();
		StringBuffer airportCodeError = new StringBuffer();
		StringBuffer flightTimeError = new StringBuffer();
		StringBuffer shortAirportNameError = new StringBuffer();
		StringBuffer longAirportNameError = new StringBuffer();
				
		errorPassengerList = errorPassengerList.stream()
				.filter(value -> value != null
				&& value.getDestAirportCode().trim().length() != 0
				&& value.getFlightId().trim().length() != 0
				&& value.getFromAirportCode().trim().length() != 0
				&& value.getPassengerId().trim().length() != 0)
				.collect(Collectors.toList());
		
		errorAirportList = errorAirportList.stream()
				.filter(value -> value != null 
				&& value.getAirportCode().trim().length() != 0
				&& value.getAirportName().trim().length() != 0
				&& value.getLatitude() != 0
				&& value.getLongitude() != 0)
				.collect(Collectors.toList());
		
		for (Passenger passenger : errorPassengerList) {
			passenger.setFlightId(passenger.getFlightId().toUpperCase());
			passenger.setPassengerId(passenger.getPassengerId().toUpperCase());
			passenger.setFromAirportCode(passenger.getFromAirportCode().toUpperCase());
			passenger.setDestAirportCode(passenger.getDestAirportCode().toUpperCase());
			
			if(!Pattern.matches(Constants.FLIGHT_ID_PATTERN, passenger.getFlightId())) {
				correctData = errorCorrection(noErrorPassengerList, null, passenger.getFlightId(), "flightId");
				if(!correctData.isEmpty()) {
					ErrorData e = new ErrorData();
					e.setCorrectData(correctData);
					e.setErrorData(passenger.getFlightId());
					flightIdErrorList.add(e);
					passenger.setFlightId(correctData);
				}else {
					passengerDeleteList.add(passenger);
				}
			}
			
			if(!Pattern.matches(Constants.PASSENGER_ID_PATTERN, passenger.getPassengerId())) {
				correctData = errorCorrection(noErrorPassengerList, null, passenger.getPassengerId(), "passengerId");
				if(!correctData.isEmpty()) {
					ErrorData e = new ErrorData();
					e.setCorrectData(correctData);
					e.setErrorData(passenger.getPassengerId());
					passengerIdErrorList.add(e);
					passenger.setPassengerId(correctData);
				}else {
					passengerDeleteList.add(passenger);
				}
			}
			
			if(!Pattern.matches(Constants.AIRPORT_CODE_PATTERN, passenger.getFromAirportCode())) {			
				correctData = errorCorrection(null, errorAirportList, passenger.getFromAirportCode(), "");
				if(!correctData.isEmpty()) {
					ErrorData e = new ErrorData();
					e.setCorrectData(correctData);
					e.setErrorData(passenger.getFromAirportCode());
					airportCodeErrorList.add(e);
					passenger.setFromAirportCode(correctData);
				}else {
					passengerDeleteList.add(passenger);
				}
			}
			
			if(!Pattern.matches(Constants.AIRPORT_CODE_PATTERN, passenger.getDestAirportCode())) {
				correctData = errorCorrection(null, errorAirportList, passenger.getDestAirportCode(), "");
				if(!correctData.isEmpty()) {
					ErrorData e = new ErrorData();
					e.setCorrectData(correctData);
					e.setErrorData(passenger.getDestAirportCode());
					airportCodeErrorList.add(e);
					passenger.setDestAirportCode(correctData);
				}else {
					passengerDeleteList.add(passenger);
				}
			}
			
			if(!(String.valueOf(passenger.getTotalFlightTime()).length() > 0 && String.valueOf(passenger.getTotalFlightTime()).length() <= 4)) {
				flightTimeError.append(String.valueOf(passenger.getTotalFlightTime()));
				flightTimeError.append(",");			
			}
		}
		
		for (Airport airport : errorAirportList) {
			airport.setAirportCode(airport.getAirportCode().toUpperCase());

			if(!Pattern.matches(Constants.AIRPORT_CODE_PATTERN, String.valueOf(airport.getAirportCode()))) {
				airportCodeError.append(String.valueOf(airport.getAirportCode()));
				airportCodeError.append(",");
				airportDeleteList.add(airport);
			}
			
			if(!(airport.getAirportName().length() >= 3)) {
				shortAirportNameError.append(airport.getAirportName());
				shortAirportNameError.append(",");				
			}
			
			if(!(airport.getAirportName().length() <= 20)){
				longAirportNameError.append(airport.getAirportName());
				airportDeleteList.add(airport);
				airport.setAirportName(airport.getAirportName().substring(0, 20));				
			}	
		}

		Map output = new HashMap();
		output.put("flightIdError", flightIdErrorList);
		output.put("passengerIdError", passengerIdErrorList);
		output.put("airportCodeError", airportCodeErrorList);
		output.put("flightTimeError", flightTimeError.toString());
		output.put("shortAirportNameError", shortAirportNameError.toString());
		output.put("longAirportNameError", longAirportNameError.toString());
		output.put("passengerList", errorPassengerList);
		output.put("errorAirportList", errorAirportList);
			
		return output;
		
	}
	
	
	public static String errorCorrection(List<Passenger> passengerList, List<Airport> airportList, String errorData, String dataType) {
		int minD = 9999;
		String correctData = "";
		int d = 0 ;

		if(airportList != null) {
		    for (Airport a : airportList) {
		        d = LevenshteinDistance.getDefaultInstance().apply(a.getAirportCode(), errorData);	        
		        if(minD > d) {
		        	minD = d;
		        	correctData = a.getAirportCode();
		        }
		    }
		}
		if(passengerList != null) {
			for (Passenger p : passengerList) {
				if(dataType.equalsIgnoreCase("passengerId")) {
					d = LevenshteinDistance.getDefaultInstance().apply(p.getPassengerId(), errorData);
					if(minD > d) {
			        	minD = d;
			        	correctData = p.getPassengerId();
			        }
				}else if(dataType.equalsIgnoreCase("flightId")) {
					d = LevenshteinDistance.getDefaultInstance().apply(p.getFlightId(), errorData);
					if(minD > d) {
			        	minD = d;
			        	correctData = p.getFlightId();
			        }
				}
		    }
		}
		return correctData;
	}
	
}
